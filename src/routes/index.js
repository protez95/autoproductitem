import PUBLIC_ROUTES from './PublicRoutes';
import USER_ROUTES from './UserRoutes';
import ADMIN_ROUTES from './AdminRoutes';
import OTHER_ROUTES from './OtherRoutes';

export default { PUBLIC_ROUTES, USER_ROUTES, ADMIN_ROUTES, OTHER_ROUTES };
